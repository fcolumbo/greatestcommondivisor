# Greatest Common Divisor (GCD)

## What is GCD?
In mathematics, the greatest common divisor (gcd) of two or more integers, which are not all zero, is the largest 
positive integer that divides each of the integers. For example, the gcd of 8 and 12 is 4

## Why a repository?

This repository has been created because an interest in stackoverflow question: 
https://stackoverflow.com/questions/63431524/greatest-common-divisor-gcd-by-euclidean-algorithm-in-java

The question is:
> I wrote this code for Greatest Common Divisor- GCD. I subtract smaller integer from bigger integer, 
then I remove biggest integer and subtract smaller integer from bigger integer again until two integers are equal and it's result. I'd like to know if this is correct and if it can be made simpler? Thank You.


## What is in the repository?

This repository contains various solutions and testing. It is being used as an ad-hoc training exercise
by the repository owner.

The package questionersCode contains the (slightly modified) code from KamilP who asked the question 
about getting the GCD.

The package Solution01 contains a method of calculating GCD using Object Oriented Programming principals.

The package Solution02 contains a method of calculating GCD using functional programming principals.

The package Euclid contains a method of calculating GCD using Euclid's algorithm.

The package DevilsHnd contains the (slightly modified) code from DevilsHnd who replied to KamilP on stackoverflow.

The package BinaryGCD contains the code for the Binary GCD algorithm. This is reported as being quicker than Euclid
Algorithm in the following places:

- https://iq.opengenus.org/binary-gcd-algorithm/
- https://en.wikipedia.org/wiki/Greatest_common_divisor

however, my tests have not proved this.

The tests do not require that the GCD is recorded or printed to screen, as this would add considerable time. Unit 
tests have been created so that each solution has been tested to provide the correct GCD,

## List Of Tests and results
-First Test - used questionersCode, solution01 and solution02. Proved questionersCode was by far superior.

-Second Test - used questionersCode only with 1,000,000 random pairs of numbers.

-Third Test - used questionersCode and Euclid. Euclid is the quicker.

-Fourth Test - used questionersCode, Euclid and DevilsHnd. DevilsHnd is slightly quicker.

-Fifth Test - used questionersCode, Euclid, DevilsHnd and Binary GCD Algorithm. DevilsHnd is this quickest.

---
## Fifth Test

### Solutions used
- questionersCode
- Euclid
- DevilsHnd
- Binary GCD Algorithm

### Test specifications:
- 3 2d arrays of random pairs of numbers.
- The arrays have a length of 1,000,000.
- First array has random numbers between 1 and 107374143.
- Second array has random numbers between 107374143 and 2147483646.
- Third array has random numbers between 1 and 2147483646.
- Test timed in miliseconds.

### Test results:
questionersCode:

- First array 251 miliseconds
- Second array 282 miliseconds
- Third array 281 miliseconds

Euclid:

- First array 168 miliseconds
- Second array 260 miliseconds
- Third array 173 miliseconds

DevilsHnd

- First array 139 miliseconds
- Second array 262 miliseconds
- Third array 158 miliseconds

Binary GCD Algorithm

- First array 196 miliseconds
- Second array 291 miliseconds
- Third array 215 miliseconds

### Test conclusion:

The DevilsHnd code is slightly quicker

---
## Fourth Test

### Solutions used
- questionersCode
- Euclid
- DevilsHnd

### Test specifications:
- 3 2d arrays of random pairs of numbers.
- The arrays have a length of 1,000,000.
- First array has random numbers between 1 and 107374143.
- Second array has random numbers between 107374143 and 2147483646.
- Third array has random numbers between 1 and 2147483646.
- Test timed in miliseconds.

### Test results:
questionersCode:

- First array 270 miliseconds
- Second array 281 miliseconds
- Third array 284 miliseconds

Euclid:

- First array 160 miliseconds
- Second array 176 miliseconds
- Third array 173 miliseconds

DevilsHnd

- First array 138 miliseconds
- Second array 157 miliseconds
- Third array 150 miliseconds

### Test conclusion:

The DevilsHnd code is slightly quicker

---

## Third test.

### Solutions Used:
- questionersCode
- Euclid

### Test specifications:
- 3 2d arrays of random pairs of numbers.
- The arrays have a length of 1,000,000.
- First array has random numbers between 1 and 107374143.
- Second array has random numbers between 107374143 and 2147483646.
- Third array has random numbers between 1 and 2147483646.
- Test timed in miliseconds.

### Test results:
questionersCode:

- First array 273 miliseconds
- Second array 274 miliseconds
- Third array 285 miliseconds

Euclid:

- First array 155 miliseconds
- Second array 175 miliseconds
- Third array 173 miliseconds

### Test conclusion:

The Euclid beat the questionersCode.

---
## Second test.

### Solutions Used:
-questionersCode

### Test specifications:
- 3 2d arrays of random pairs of numbers.
- The arrays had a length of 1,000,000.
- First array has random numbers between 1 and 107374143.
- Second array has random numbers between 107374143 and 2147483646.
- Third array has random numbers between 1 and 2147483646.
- Test timed in miliseconds.

### Test results:
questionersCode:

- First array 260 miliseconds
- Second array 327 miliseconds
- Third array 287 miliseconds

### Test conclusion:

The questionersCode results are impressive and will be used as a benchmark for other solutions.

---

## First Test.

### Solutions used: 
- questionersCode
- solution01
- solution02.

### Test specifications:
- 3 2d arrays of random pairs of numbers.
- The arrays had a length of 10.
- First array has random numbers between 1 and 107374143.
- Second array has random numbers between 107374143 and 2147483646.
- Third array has random numbers between 1 and 2147483646.
- Test timed in miliseconds.

### Test results:
questionersCode:

- First array 0 miliseconds
- Second array 0 miliseconds
- Third array 0 miliseconds
	
Solution01:

- First array 3826 miliseconds
- Second array 64451 miliseconds
- Third array 50426 miliseconds
	
Solution02:

- First array 4146 miliseconds
- Second array 57518 miliseconds
- Thirdy array 50377 miliseconds
	
### Test conclusion:

The questionersCode solution is by far superior. solution01 and solution02 will no longer be tested.
