
package greatestcommondivisor;

import WJS.WJS;
import binaryGCD.BinaryGCD;
import devilshnd.DevilsHnd;
import euclid.Euclid;
import questionersCode.QuestionersCode;
import solution01.Solution01;
import solution02.Solution02;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Alex Martin
 */
public class GreatestCommonDivisor 
{

    public static void main(String[] args) 
    {
        QuestionersCode QC = new QuestionersCode();
        Euclid EU = new Euclid();
        DevilsHnd DH = new DevilsHnd();
        BinaryGCD BGCD = new BinaryGCD();
        WJS wjs = new WJS();
        //Solution01 S01 = new Solution01();
        //Solution02 S02 = new Solution02();
        
        int arrayLength = 1000000;
        
        int[][] bottomHalfArray = makeArrayOfRandomNumbers(arrayLength, 1, 107374143);
        int[][] topHalfArray = makeArrayOfRandomNumbers(arrayLength, 107374143, 2147483646);
        int[][] fullRangeArray = makeArrayOfRandomNumbers(arrayLength, 1, 2147483646);
        
        final long QCBOTTOMHALFARRAY = TestSolutions.TimeTest(bottomHalfArray, QC);
        System.out.println("QCBOTTOMHALFARRAY " + QCBOTTOMHALFARRAY);
//        final long S01BOTTOMHALFARRAY = TestSolutions.TimeTest(bottomHalfArray, S01);
//        System.out.println("S01BOTTOMHALFARRAY " + S01BOTTOMHALFARRAY);
//        final long S02BOTTOMHALFARRAY = TestSolutions.TimeTest(bottomHalfArray, S02);
//        System.out.println("S02BOTTOMHALFARRAY " + S02BOTTOMHALFARRAY);
        final long EUBOTTOMHALFARRAY = TestSolutions.TimeTest(bottomHalfArray, EU);
        System.out.println("EUBOTTOMHALFARRAY " + EUBOTTOMHALFARRAY);
        final long DHBOTTOMHALFARRAY = TestSolutions.TimeTest(bottomHalfArray, DH);
        System.out.println("DHBOTTOMHALFARRAY " + DHBOTTOMHALFARRAY);
        final long BGCDBOTTOMHALFARRAY = TestSolutions.TimeTest(bottomHalfArray, BGCD);
        System.out.println("BGCDBOTTOMHALFARRAY " + BGCDBOTTOMHALFARRAY);
        final long WJSBOTTOMHALFARRAY = TestSolutions.TimeTest(bottomHalfArray, wjs);
        System.out.println("WJSBOTTOMHALFARRAY " + WJSBOTTOMHALFARRAY);
        
        final long QCTOPHALFARRAY = TestSolutions.TimeTest(topHalfArray, QC);
        System.out.println("QCTOPHALFARRAY " + QCTOPHALFARRAY);
//        final long S01TOPHALFARRAY = TestSolutions.TimeTest(topHalfArray, S01);
//        System.out.println("S01TOPHALFARRAY " + S01TOPHALFARRAY);
//        final long S02TOPHALFARRAY = TestSolutions.TimeTest(topHalfArray, S02);
//        System.out.println("S02TOPHALFARRAY " + S02TOPHALFARRAY);
        final long EUTOPHALFARRAY = TestSolutions.TimeTest(topHalfArray, EU);
        System.out.println("EUTOPHALFARRAY " + EUTOPHALFARRAY);
        final long DHTOPHALFARRAY = TestSolutions.TimeTest(topHalfArray, DH);
        System.out.println("DHTOPHALFARRAY " + DHTOPHALFARRAY);
        final long BGCDTOPHALFARRAY = TestSolutions.TimeTest(topHalfArray, BGCD);
        System.out.println("BGCDTOPHALFARRAY " + BGCDTOPHALFARRAY);
        final long WJSTOPHALFARRAY = TestSolutions.TimeTest(topHalfArray, wjs);
        System.out.println("WJSTOPHALFARRAY " + WJSTOPHALFARRAY);
        
        final long QCFULLRANGEARRAY = TestSolutions.TimeTest(fullRangeArray, QC);
        System.out.println("QCFULLRANGEARRAY " + QCFULLRANGEARRAY);
//        final long S01FULLRANGEARRAY = TestSolutions.TimeTest(fullRangeArray, S01);
//        System.out.println("S01FULLRANGEARRAY " + S01FULLRANGEARRAY);
//        final long S02FULLRANGEARRAY = TestSolutions.TimeTest(fullRangeArray, S02);
//        System.out.println("S02FULLRANGEARRAY " + S02FULLRANGEARRAY);
        final long EUFULLRANGEARRAY = TestSolutions.TimeTest(fullRangeArray, EU);
        System.out.println("EUFULLRANGEARRAY " + EUFULLRANGEARRAY);
        final long DHFULLRANGEARRAY = TestSolutions.TimeTest(fullRangeArray, DH);
        System.out.println("DHFULLRANGEARRAY " + DHFULLRANGEARRAY);
        final long BGCDFULLRANGEARRAY = TestSolutions.TimeTest(fullRangeArray, BGCD);
        System.out.println("BGCDFULLRANGEARRAY " + BGCDFULLRANGEARRAY);
        final long WJSFULLRANGEARRAY = TestSolutions.TimeTest(fullRangeArray, wjs);
        System.out.println("WJSFULLRANGEARRAY " + WJSFULLRANGEARRAY);
        
    }
    public static int[][] makeArrayOfRandomNumbers(int arrayLength, int minNumber, int maxNumber)
    {
        int[][] tempArray = new int[arrayLength][2];
        for(int count = 0; count < arrayLength;count++)
        {
            tempArray[count][0] = ThreadLocalRandom.current().nextInt(minNumber, maxNumber + 1);
            tempArray[count][1] = ThreadLocalRandom.current().nextInt(minNumber, maxNumber + 1);
        }
        return tempArray;
    }
    
}
