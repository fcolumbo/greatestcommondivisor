
package greatestcommondivisor;

import WJS.WJS;
import binaryGCD.BinaryGCD;
import devilshnd.DevilsHnd;
import euclid.Euclid;
import questionersCode.QuestionersCode;
import solution01.Solution01;
import solution02.Solution02;

/**
 *
 * @author Alex
 */
public class TestSolutions 
{
    public static long TimeTest(int [][] randomArray, QuestionersCode QC)
    {
        final long startTime = System.currentTimeMillis();
        for (int[] randomArray1 : randomArray) 
        {
            QC.calculateGCD(randomArray1[0], randomArray1[1]);
        }
        final long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
    public static long TimeTest(int [][] randomArray, Solution01 S01)
    {
        final long startTime = System.currentTimeMillis();
        for (int[] randomArray1 : randomArray) 
        {
            S01.calculateGCD(randomArray1[0], randomArray1[1]);
        }
        final long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
    public static long TimeTest(int [][] randomArray, Solution02 S02)
    {
        final long startTime = System.currentTimeMillis();
        for (int[] randomArray1 : randomArray) 
        {
            S02.calculateGCD(randomArray1[0], randomArray1[1]);
        }
        final long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
    public static long TimeTest(int [][] randomArray, Euclid EU)
    {
        final long startTime = System.currentTimeMillis();
        for (int[] randomArray1 : randomArray) 
        {
            EU.calculateGCD(randomArray1[0], randomArray1[1]);
        }
        final long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
    public static long TimeTest(int [][] randomArray, DevilsHnd DH)
    {
        final long startTime = System.currentTimeMillis();
        for (int[] randomArray1 : randomArray) 
        {
            DH.calculateGCD(randomArray1[0], randomArray1[1]);
        }
        final long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
    public static long TimeTest(int [][] randomArray, BinaryGCD BGCD)
    {
        final long startTime = System.currentTimeMillis();
        for (int[] randomArray1 : randomArray) 
        {
            BGCD.calculateGCD(randomArray1[0], randomArray1[1]);
        }
        final long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
    public static long TimeTest(int [][] randomArray, WJS wjs)
    {
        final long startTime = System.currentTimeMillis();
        for (int[] randomArray1 : randomArray) 
        {
            wjs.calculateGCD(randomArray1[0], randomArray1[1]);
        }
        final long endTime = System.currentTimeMillis();
        return endTime - startTime;
    }
}
