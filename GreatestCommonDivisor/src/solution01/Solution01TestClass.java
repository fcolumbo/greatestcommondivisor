
package solution01;

/**
 *
 * @author Alex Martin
 */
public class Solution01TestClass 
{
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // vars
    private final int STARTINGNUMBER;
    private int currentNumber;
    private int modulus = 1;
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // constructor
    public Solution01TestClass(int number)
    {
        this.STARTINGNUMBER = number;
        setCurrentNumber(this.STARTINGNUMBER);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // methods
    public int getNextDivisor()
    {
        incrementModulus();
        while(!modulusEqualsZero(this.STARTINGNUMBER, this.modulus))
        {
            incrementModulus();
        }
        setCurrentNumber();
        return this.currentNumber;
    }
    private boolean modulusEqualsZero(int number, int modulus)
    {
        return number%modulus == 0;
    }
    private void incrementModulus()
    {
        this.modulus++;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // getters
    public int getCurrentNumber()
    {
        return this.currentNumber;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    // setters
    private void setCurrentNumber()
    {
        this.currentNumber = this.STARTINGNUMBER/this.modulus;
    }
    private void setCurrentNumber(int number)
    {
        this.currentNumber = number;
    }
}
