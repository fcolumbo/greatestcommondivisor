
package solution01;

/**
 *
 * @author Alex Martin
 */
public class Solution01 
{

    
    public Solution01() 
    {
        
    }
    public int calculateGCD(int firstInteger, int secondInteger)
    {
        boolean GCDFound = false;

        Solution01TestClass firstClass = new Solution01TestClass(firstInteger);
        Solution01TestClass secondClass = new Solution01TestClass(secondInteger);

        while(!GCDFound)
        {
            if(isHigher(firstClass.getCurrentNumber(), secondClass.getCurrentNumber()))
            {
                firstClass.getNextDivisor();
            }
            else
            {
                secondClass.getNextDivisor();
            }
            if(isEqual(firstClass.getCurrentNumber(), secondClass.getCurrentNumber()))
            {
                GCDFound = true;

            }
        }
        return firstClass.getCurrentNumber();
    }
    public static boolean isHigher(int numberOne, int numberTwo)
    {
        return numberOne > numberTwo;
    }
    public static boolean isEqual(int numberOne, int numberTwo)
    {
        return numberOne == numberTwo;
    }
}
