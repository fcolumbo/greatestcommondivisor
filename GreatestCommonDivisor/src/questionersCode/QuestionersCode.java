/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionersCode;

/**
 *
 * @author KamilP
 * @see https://stackoverflow.com/questions/63431524/greatest-common-divisor-gcd-by-euclidean-algorithm-in-java
 */
public class QuestionersCode 
{
    public QuestionersCode()
    {
        
    }
        
    public int calculateGCD(int firstInteger, int secondInteger)
    {
        if (firstInteger>secondInteger) 
        {
            int result=firstInteger-secondInteger;
            while (firstInteger != secondInteger) 
            {
                if (secondInteger > result) 
                {
                    firstInteger = secondInteger;
                    secondInteger = result;
                    result = firstInteger - secondInteger;
                } 
                else 
                {
                    firstInteger = result;
                    result = firstInteger - secondInteger;
                }
            }
           return firstInteger;
        }
        else 
        {
            int result=secondInteger-firstInteger;
            while (secondInteger!=firstInteger) 
            {
                if (firstInteger>result)  
                {
                    secondInteger=firstInteger;
                    firstInteger=result;
                    result=secondInteger-firstInteger;
                }
                else 
                {
                    secondInteger=result;
                    result=secondInteger-firstInteger;
                }
            }
            return secondInteger;
        }
    }

       
}
