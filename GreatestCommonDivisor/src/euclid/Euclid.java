
package euclid;

/**
 *
 * @author Alex Martin
 */
public class Euclid 
{
    public Euclid()
    {
        
    }
    public int calculateGCD(int firstInteger, int secondInteger)
    {
        boolean GCDFound = false;
        int highNumber, lowNumber, quotient, remainder, GCD = 1;
        
         if(isHigher(firstInteger, secondInteger))
        {
            highNumber = firstInteger;
            lowNumber = secondInteger;
        }
        else
        {
            highNumber = secondInteger;
            lowNumber = firstInteger;
        }
         
         while(!GCDFound)
         {
            quotient = highNumber / lowNumber;
            remainder = Math.floorMod(highNumber, lowNumber);
            if(remainder == 0)
            {
                GCDFound = true;
                GCD = lowNumber;
                break;
            }
            else
            {
                highNumber = lowNumber;
                lowNumber = remainder;
            }
         }
         return GCD;
    }
    private boolean isHigher(int numberOne, int numberTwo)
    {
        return numberOne > numberTwo;
    }
}
