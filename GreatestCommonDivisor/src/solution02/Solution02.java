
package solution02;

/**
 *
 * @author Alex Martin
 */
public class Solution02 
{
    public Solution02()
    {
        
    }
    public int calculateGCD(int firstNumber, int secondNumber)
    {
        int firstNumberDivisor = firstNumber, secondNumberDivisor = secondNumber, 
                firstNumberModulus = 1, secondNumberModulus = 1;
        boolean GCDFound = false;
        
        while(!GCDFound)
        {
            if(isEqual(firstNumberDivisor, secondNumberDivisor))
            {
                GCDFound = true;
                break;
            }
            if(isHigher(firstNumberDivisor, secondNumberDivisor))
            {
                firstNumberModulus++;
                while(!modulusEqualsZero(firstNumber, firstNumberModulus))
                {
                    firstNumberModulus++;
                }
                firstNumberDivisor = firstNumber / firstNumberModulus;
            }
            else
            {
                secondNumberModulus++;
                while(!modulusEqualsZero(secondNumber, secondNumberModulus))
                {
                    secondNumberModulus++;
                }
                secondNumberDivisor = secondNumber / secondNumberModulus;
            }
        }
        return firstNumberDivisor;
    }
    private boolean modulusEqualsZero(int number, int modulus)
    {
        return number%modulus == 0;
    }
    private boolean isEqual(int numberOne, int numberTwo)
    {
        return numberOne == numberTwo;
    }
    private boolean isHigher(int numberOne, int numberTwo)
    {
        return numberOne > numberTwo;
    }
    
}
