/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package devilshnd;

/**
 *
 * @author DevilsHnd
 * @see https://stackoverflow.com/questions/63431524/greatest-common-divisor-gcd-by-euclidean-algorithm-in-java/63440602#63440602
 */
public class DevilsHnd 
{
    public DevilsHnd()
    {
        
    }
    public int calculateGCD(int firstInteger, int secondInteger)
    {
        int numerator, denominator;
        if(isHigher(firstInteger, secondInteger))
        {
            numerator = firstInteger;
            denominator = secondInteger;
        }
        else
        {
            numerator = secondInteger;
            denominator = firstInteger;
        }
        if (numerator == 0 || denominator == 0) 
        {
            System.err.println("greatestCommonDivisor() Method Error! A value of "
                             + "zero (0) can not be supplied as an argument!");
            return -1;
        }
        int num = numerator;
        num = Math.abs(numerator);          // Ensure an absolute value
        int gcd = Math.abs(denominator);    // Ensure an absolute value
        int temp = num % gcd;
        while (temp > 0) 
        {
            num = gcd;
            gcd = temp;
            temp = num % gcd;
        }
        return gcd;
    }
    private boolean isHigher(int numberOne, int numberTwo)
    {
        return numberOne > numberTwo;
    }
}
