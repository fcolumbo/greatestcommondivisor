/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WJS;

/**
 *
 * @author WJS
 * @see https://stackoverflow.com/questions/63431524/greatest-common-divisor-gcd-by-euclidean-algorithm-in-java/63440602
 */
public class WJS 
{
    public WJS()
    {
        
    }
    public int calculateGCD(int firstInteger, int secondInteger)
    {
        int highNumber, lowNumber;
        
         if(isHigher(firstInteger, secondInteger))
        {
            highNumber = firstInteger;
            lowNumber = secondInteger;
        }
        else
        {
            highNumber = secondInteger;
            lowNumber = firstInteger;
        }
         return gcd(highNumber, lowNumber);
    }
    public static int gcd(int r, int s) {
    if (s != 0) {
        return gcd(s, r % s);
    }
    return r > 0 ? r : -r;
}
    private boolean isHigher(int numberOne, int numberTwo)
    {
        return numberOne > numberTwo;
    }
}
