/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euclid;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author HP
 */
public class EuclidTest {
    
    public EuclidTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculateGCD method, of class Euclid.
     */
    @Test
    public void testCalculateGCD01() {
        System.out.println("calculateGCD: 1151856 - 1368. Test large number first");
        int a = 1151856;
        int b = 1368;
        Euclid instance = new Euclid();
        int expResult = 1368;
        int result = instance.calculateGCD(a, b);
        assertEquals(expResult, result);
    }
    @Test
    public void testCalculateGCD02() {
        System.out.println("calculateGCD: 1368 - 1151856. Test large number second.");
        int a = 1368;
        int b = 1151856;
        Euclid instance = new Euclid();
        int expResult = 1368;
        int result = instance.calculateGCD(a, b);
        assertEquals(expResult, result);
    }
    @Test
    public void testCalculateGCD03() {
        System.out.println("calculateGCD: 2147483646 - 1368768. Test max int number.");
        int a = 2147483646;
        int b = 1368768;
        Euclid instance = new Euclid();
        int expResult = 6;
        int result = instance.calculateGCD(a, b);
        assertEquals(expResult, result);
    }
    @Test
    public void testCalculateGCD04() {
        System.out.println("calculateGCD: 48 - 12. Test lowest number is GCD");
        int a = 48;
        int b = 12;
        Euclid instance = new Euclid();
        int expResult = 12;
        int result = instance.calculateGCD(a, b);
        assertEquals(expResult, result);
    }
    @Test
    public void testCalculateGCD05() {
        System.out.println("calculateGCD: 48 - 13. Test when there is no GCD - answer is 1");
        int a = 48;
        int b = 13;
        Euclid instance = new Euclid();
        int expResult = 1;
        int result = instance.calculateGCD(a, b);
        assertEquals(expResult, result);
    }
}
