/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package solution01;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author HP
 */
public class Solution01Test {
    
    public Solution01Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculateGCD method, of class Solution01.
     */
    @Test
    public void testCalculateGCD01() {
        System.out.println("calculateGCD: 1151856 - 1368. Test large number first");
        int a = 1151856;
        int b = 1368;
        Solution01 instance = new Solution01();
        int expResult = 1368;
        int result = instance.calculateGCD(a, b);
        assertEquals(expResult, result);
    }
    @Test
    public void testCalculateGCD02() {
        System.out.println("calculateGCD: 1368 - 1151856. Test large number second.");
        int a = 1368;
        int b = 1151856;
        Solution01 instance = new Solution01();
        int expResult = 1368;
        int result = instance.calculateGCD(a, b);
        assertEquals(expResult, result);
    }
    @Test
    public void testCalculateGCD03() {
        System.out.println("calculateGCD: 2147483646 - 1368768. Test max int number.");
        int a = 2147483646;
        int b = 1368768;
        Solution01 instance = new Solution01();
        int expResult = 6;
        int result = instance.calculateGCD(a, b);
        assertEquals(expResult, result);
    }
    @Test
    public void testCalculateGCD04() {
        System.out.println("calculateGCD: 48 - 12. Test lowest number is GCD");
        int a = 48;
        int b = 12;
        Solution01 instance = new Solution01();
        int expResult = 12;
        int result = instance.calculateGCD(a, b);
        assertEquals(expResult, result);
    }
    @Test
    public void testCalculateGCD05() {
        System.out.println("calculateGCD: 48 - 13. Test when there is no GCD - answer is 1");
        int a = 48;
        int b = 13;
        Solution01 instance = new Solution01();
        int expResult = 1;
        int result = instance.calculateGCD(a, b);
        assertEquals(expResult, result);
    }
    /**
     * Test of isHigher method, of class Solution01.
     */
    @Test
    public void testIsHigher01() {
        System.out.println("isHigher: 55 - 10");
        int numberOne = 55;
        int numberTwo = 10;
        boolean expResult = true;
        boolean result = Solution01.isHigher(numberOne, numberTwo);
        assertEquals(expResult, result);
    }
    @Test
    public void testIsHigher02() {
        System.out.println("isHigher: 10 - 55");
        int numberOne = 10;
        int numberTwo = 55;
        boolean expResult = false;
        boolean result = Solution01.isHigher(numberOne, numberTwo);
        assertEquals(expResult, result);
    }
    /**
     * Test of isEqual method, of class Solution01.
     */
    @Test
    public void testIsEqua01l() {
        System.out.println("isEqual: 10 - 10");
        int numberOne = 10;
        int numberTwo = 10;
        boolean expResult = true;
        boolean result = Solution01.isEqual(numberOne, numberTwo);
        assertEquals(expResult, result);
    }
    @Test
    public void testIsEqua02l() {
        System.out.println("isEqual: 100 - 10");
        int numberOne = 100;
        int numberTwo = 10;
        boolean expResult = false;
        boolean result = Solution01.isEqual(numberOne, numberTwo);
        assertEquals(expResult, result);
    }
    @Test
    public void testIsEqua03l() {
        System.out.println("isEqual: 10 - 100");
        int numberOne = 10;
        int numberTwo = 100;
        boolean expResult = false;
        boolean result = Solution01.isEqual(numberOne, numberTwo);
        assertEquals(expResult, result);
    }
}
